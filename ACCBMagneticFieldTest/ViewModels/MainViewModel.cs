﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCBMagneticFieldTest
{
    public class MainViewModel
    {
        public NICard NICard
        {
            get { return AppCtrl.Instance.NICard; }
        }

        #region Commands

        public RelayCommand CommandToggleRecord
        {
            get
            {
                if (_commandToggleRecord == null)
                    _commandToggleRecord = new RelayCommand(
                        new Action<object>((arg) => NICard.StartRecord()),
                        new Predicate<object>((arg) => NICard.CanStart));

                return _commandToggleRecord;
            }
        }
        private RelayCommand _commandToggleRecord;

        #endregion
    }
}
