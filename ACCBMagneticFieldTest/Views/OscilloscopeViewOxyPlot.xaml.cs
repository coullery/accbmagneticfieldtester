﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ACCBMagneticFieldTest
{
    /// <summary>
    /// Logique d'interaction pour Oscilloscope.xaml
    /// </summary>
    public partial class OscilloscopeViewOxyPlot : UserControl
    {
        public OscilloscopeViewOxyPlot()
        {
            InitializeComponent();

        }

        public NICard NICard
        {
            get { return (NICard)GetValue(NICardProperty); }
            set { SetValue(NICardProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NICard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NICardProperty =
            DependencyProperty.Register("NICard", typeof(NICard), typeof(OscilloscopeViewOxyPlot), new PropertyMetadata(null));

    }
}
