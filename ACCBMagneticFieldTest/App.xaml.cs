﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ACCBMagneticFieldTest
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void StartApp(object sender, StartupEventArgs e)
        {
            AppCtrl.Instance.Initialize();
        }
    }
}
