﻿using NationalInstruments.DAQmx;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

 

namespace ACCBMagneticFieldTest
{
    

    public class NICard : INotifyPropertyChanged
    {
        #region Constructor and initialize
        private bool trigged = false;
        private bool displayed = false;
        private int count = 0;
        private double [] table = new double[250];
        private bool good_field = false;
        private int index = 0;
        private double[] start_peak = new double[10];

        public NICard()
        {
            ComputingUnit = new ComputingUnit();
            acquisitionData = new List<double>();
            AverageRate = null;
            RemainingDuration = 0;
            WatchFrequency = 40;
            SelectedDuration = 5;


            //Creation of the OxyPlot with 2 invisibles axis and 1 serie with the ONEOF blue.
            OxyPlotModel = new PlotModel
            {
                Background = OxyColors.Transparent,
                PlotAreaBorderThickness = new OxyThickness(1),
                PlotAreaBorderColor = OxyColors.White,
            };
            OxyPlotModel.Axes.Add(new OxyPlot.Axes.LinearAxis
            {
                Maximum = 5,
                Minimum = 0,
                Position = OxyPlot.Axes.AxisPosition.Left,
                MinimumPadding = 0,
                MaximumPadding = 0,
                IsAxisVisible = true,
                TicklineColor = OxyColors.White,
                TextColor = OxyColors.White
            });
            OxyPlotModel.Axes.Add(new OxyPlot.Axes.LinearAxis
            {
                Position = OxyPlot.Axes.AxisPosition.Bottom,
                MinimumPadding = 0,
                MaximumPadding = 0,
                IsAxisVisible = true,
                TicklineColor = OxyColors.White,
                TextColor = OxyColors.White
            });
            OxyPlotModel.Series.Add(new OxyPlot.Series.LineSeries() { Color = OxyColor.FromRgb(0, 155, 237) });
            OxyPlotModel.Series.Add(new OxyPlot.Series.LineSeries() { Color = OxyColor.FromRgb(37, 245, 14) });
            OxyPlotModel.Series.Add(new OxyPlot.Series.LineSeries() { Color = OxyColor.FromRgb(255, 0, 0) });
        }

        public void Initialize()
        {
            AcquisitionFrequency = 10000;
            AcquisitionDurationMS = 100;

            //Lister les canaux disponibles
            string[] Channels = DaqSystem.Local.GetPhysicalChannels(PhysicalChannelTypes.AI, PhysicalChannelAccess.External);

            if (Channels.Length == 0)
            {
                MessageBox.Show("L'appareil de mesure n'est pas branché, veuillez le brancher avant le lancement du progamme.");
                return;
            }

            if (!Channels.Contains("Dev1/ai0"))
            {
                MessageBox.Show("Le nom de l'appareil de mesure n'est pas correct. Il doit être nommé Dev1. Pour le renommer : " +
                    "lancer le programme NI MAX, modifier le nom de la carte sous Périphériques et interfaces.");
                return;
            }

            //Créer une tâche de mesure
            acqTask = new NationalInstruments.DAQmx.Task();

            //Créer les canaux de mesure
            acqTask.AIChannels.CreateVoltageChannel("Dev1/ai0", "signal", AITerminalConfiguration.Rse, -10, +10, AIVoltageUnits.Volts);

            //Configuration de la fréquence d'aquisition
            //La tâche est exécutée en continue.
            acqTask.Timing.ConfigureSampleClock(string.Empty,
                AcquisitionFrequency,
                SampleClockActiveEdge.Rising,
                SampleQuantityMode.ContinuousSamples);

            //Contrôle de la tâche de mesure
            acqTask.Control(TaskAction.Verify);

            //Liaison du lecteur au flux de données
            singleChReader = new AnalogSingleChannelReader(acqTask.Stream);

            //Au départ, on est en mode affichage uniquement et l'utilisateur peut démarrer l'enregistreement.
            MustRecord = false;
            CanStart = true;
        }
        #endregion

        #region Properties

        public int AcquisitionFrequency { get; protected set; }
        public int AcquisitionDurationMS { get; protected set; }
        public int AcquisitionNbSamples { get { return AcquisitionFrequency * AcquisitionDurationMS / 1000; } }
        public PlotModel OxyPlotModel { get; set; }

        public double WatchFrequency { get; set; }

        public bool MustSave { get; set; }

        public int SelectedDuration { get; set; }
        public bool MustRecord { get; set; }
        public string ExportFileName { get; set; }

        public ComputingUnit ComputingUnit { get; set; }

        public bool CanStart
        {
            get
            {
                return canStart;
            }
            set
            {
                if (canStart != value)
                {
                    canStart = value;
                    DoPropertyChanged(nameof(CanStart));
                }
            }
        }
        private bool canStart;

        public bool Saving
        {
            get
            {
                return saving;
            }
            set
            {
                if (saving != value)
                {
                    saving = value;
                    DoPropertyChanged(nameof(Saving));
                }
            }
        }
        private bool saving;

        public double? AverageRate
        {
            get
            {
                return averageRate;
            }
            set
            {
                if (averageRate != value)
                {
                    averageRate = value;
                    DoPropertyChanged(nameof(AverageRate));
                }
            }
        }
        private double? averageRate;

        public double RemainingDuration
        {
            get
            {
                return remaingDuration;
            }
            set
            {
                if (remaingDuration != value)
                {
                    remaingDuration = value;
                    DoPropertyChanged(nameof(RemainingDuration));
                }
            }
        }
        private double remaingDuration;

        #endregion

        #region Public methods

        public void StartAcq()
        {
            //Démarre l'acquisition d'un nombre d'échantillons donnés, puis appel le callback lorsque le buffer est plein.
            dataSingleChannel = new double[AcquisitionNbSamples];
            IAsyncResult handle = singleChReader.BeginMemoryOptimizedReadMultiSample(AcquisitionNbSamples, AcquisitionComplete, null, dataSingleChannel);
        }

        public void StartRecord()
        {
            count = 0;
            trigged = false;
            displayed = false;
            good_field = false;
            ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.Clear();
            ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[2]).Points.Clear();
            oxyPlotValues.Clear();
             //AppCtrl.Instance.MainWin.Oscilloscope.OxyPlotView.InvalidatePlot(true);
            
            /*
            if (MustRecord)
            {
                //Annulation de la mesure
                MustRecord = false;
                AverageRate = null;
                RemainingDuration = 0;
            }
            else
            {
                //On efface l'affichage du graphe
                ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.Clear();
                AppCtrl.Instance.MainWin.Oscilloscope.OxyPlotView.InvalidatePlot();
                RemainingDuration = SelectedDuration;
                MustRecord = true;
            }
            AverageRate = null;
            acquisitionData.Clear();
            */
        }

        #endregion

        #region Private methods
        private void AcquisitionComplete(IAsyncResult ar)
        {
            Console.Write("Ici");
            //Lecture des données d'acquisition
            int actualnbSamples = AcquisitionNbSamples;
            dataSingleChannel = singleChReader.EndMemoryOptimizedReadMultiSample(ar, out actualnbSamples);

            if (MustRecord)
            {
                //Enregistrement des données
                acquisitionData.AddRange(dataSingleChannel);

                //Mise à jour du temps restant
                RemainingDuration -= (double)AcquisitionDurationMS / 1000;

                if (RemainingDuration <= 0)
                {
                    try
                    {
                        //On va exporter les données, comme cette tâche est longue, on met la tâche en arrêt.
                        acqTask.Stop();

                        //Calcul de la valeur de marche
                        if (ComputingUnit.CalculateAvgRate(acquisitionData, AcquisitionFrequency, WatchFrequency, out double rate))
                            AverageRate = rate;
                        else
                            AverageRate = null;

                        //Export des données
                        if (MustSave)
                        {
                            Saving = true;
                            CanStart = false;
                            StartExport();
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("An error occured during rate calculation : \n" + ex.Message);
                        AverageRate = null;
                    }
                    finally
                    {
                        MustRecord = false;
                        RemainingDuration = 0;
                    }
                }
            }
            else
            {
                //Si on est pas en mode enregistrement, on affiche les données.
                //Trouver le premier flanc montant pour affichage triggé.
                double triggerLevel_1 = 2.6;    //Volt
                double triggerLevel_2 = 2.4;

                int j = 0;
                for (int i = 0; i < dataSingleChannel.Length; i++)
                {
                    if (trigged && oxyPlotValues.Count < 100)
                        oxyPlotValues.Add(new DataPoint(i, dataSingleChannel[i])); //affichage en temps réel
                    else if (trigged && !displayed)
                    {
                        ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.Clear();
                        ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[2]).Points.Clear();
                        if (oxyPlotValues.Max(value => value.Y) > 3.5)
                        {
                            ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.AddRange(oxyPlotValues);
                        }
                        else
                        {
                            ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[2]).Points.AddRange(oxyPlotValues);
                        }
                        displayed = true;
                    }

                    if (dataSingleChannel[i] < triggerLevel_1 && dataSingleChannel[i] > triggerLevel_2)
                    {
                        continue;
                    }

                    // Trig !
                    trigged = true;

                    

                }
                


                

                //if (sampleStart > 500)
                //  sampleStart -= 500;
                //Construction liste de points OxyPlot

                //oxyPlotValues.Clear();

                //On affiche les données à partir du trigger, un point sur 10 pour diminiuer la charge CPU


                //Update values in the chart
                /*
                if (count < 1)
                {

                    for (int i = sampleStart;  i < dataSingleChannel.Length; i = i + 1)
                    {
                        oxyPlotValues.Add(new DataPoint(i, dataSingleChannel[i])); //affichage en temps réel

                        table[i - sampleStart] = dataSingleChannel[i];

                    }
                    ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.Clear();
                    ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.AddRange(oxyPlotValues);
                }*/
                /*
                if (count == 0)
                {

                    for (int i = sampleStart; i < dataSingleChannel.Length; i = i + 1)
                    {
                        oxyPlotValues.Add(new DataPoint(i, dataSingleChannel[i])); //affichage en temps réel
                                                                                   //if (i < sampleStart)
                                                                                   //table[i+ dataSingleChannel.Length-sampleStart] = dataSingleChannel[i];

                        table[i - sampleStart] = dataSingleChannel[i];



                    }
                    ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.Clear();
                    ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.AddRange(oxyPlotValues);
                }
                else if (count == 1)
                {
                    for (int i = 0; i < dataSingleChannel.Length; i = i + 1)
                    {
                        oxyPlotValues.Add(new DataPoint(i, table[i])); //affichage en temps réel
                                                                                   //if (i < sampleStart)
                                                                                   //table[i+ dataSingleChannel.Length-sampleStart] = dataSingleChannel[i];

                        table[i - sampleStart] = dataSingleChannel[i];



                    }
                }
                */
                /*else if (count == 1)
                {
                    
                    ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[0]).Points.Clear();
                    for (int i= 0;i< dataSingleChannel.Length; i++)
                    {
                        
                        oxyPlotValues.Add(new DataPoint(i, table[i]));

                        if (table[i] > 3.5)
                        {
                            good_field = true;
                        }
                    }
                    if(good_field == true)
                        ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[1]).Points.AddRange(oxyPlotValues);
                    else
                        ((OxyPlot.Series.LineSeries)OxyPlotModel.Series[2]).Points.AddRange(oxyPlotValues);
                    int w = 0;
                }*/
                //Mise à jour affichage graphique (non bloquant)
                AppCtrl.Instance.MainWin.Oscilloscope.OxyPlotView.InvalidatePlot();
            }

            //On redémarre la tâche d'acquisition
            dataSingleChannel = new double[AcquisitionNbSamples];
            IAsyncResult handle = singleChReader.BeginMemoryOptimizedReadMultiSample(AcquisitionNbSamples, AcquisitionComplete, null, dataSingleChannel);
        }

        private void StartExport()
        {
            ThreadStart starter = () => ExportToCsv(AcquisitionFrequency, AcquisitionNbSamples, WatchFrequency, (double)AverageRate, acquisitionData);

            starter += () =>
            {
                ExportDone();
            };

            Thread _thread = new Thread(starter) { IsBackground = true };
            _thread.Start();
        }

        private void ExportDone()
        {
            acquisitionData.Clear();
            CanStart = true;
            Saving = false;

            //On redémarre la tâche d'acquisition
            dataSingleChannel = new double[AcquisitionNbSamples];
            IAsyncResult handle = singleChReader.BeginMemoryOptimizedReadMultiSample(AcquisitionNbSamples, AcquisitionComplete, null, dataSingleChannel);
        }

        private void ExportToCsv(int acquisitionFreq, int acquisitionDuration,
            double watchFreq, double avgRate, List<double> dataToSave)
        {
            try
            {
                //Construction du chemin complet
                string filePath = "Export/";

                Directory.CreateDirectory(filePath);

                if (ExportFileName != null && ExportFileName != "")
                {
                    filePath += ExportFileName;
                }
                else
                {
                    filePath += DateTime.Now.ToString("yyyyMMdd-hhmmss");
                }
                filePath += ".csv";

                //Si le fichier existe déjà on demande si il faut l'écraser
                if (File.Exists(filePath))
                {
                    MessageBoxResult msgResult = MessageBox.Show(
                        "The file already exists, do you want to overwritte it ?",
                        "Confirmation", MessageBoxButton.YesNo);
                    if (msgResult == MessageBoxResult.Yes)
                    {
                        WriteCsv(filePath, acquisitionFreq, acquisitionDuration, watchFreq, avgRate, dataToSave);
                    }
                }
                else
                    WriteCsv(filePath, acquisitionFreq, acquisitionDuration, watchFreq, avgRate, dataToSave);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured during data export :\n" + ex.Message);
            }
        }

        private void WriteCsv(string filePath, int acquisitionFreq, int acquisitionDuration,
            double watchFreq, double avgRate, List<double> dataToSave)
        {
            using (StreamWriter sw = new StreamWriter(filePath))
            {
                sw.WriteLine("Frequence acquisition;Duree acquisition;Frequence montre;Marche moyenne");
                sw.WriteLine(acquisitionFreq + ";" + acquisitionDuration + ";" + watchFreq + ";" + avgRate);
                foreach (double value in dataToSave)
                {
                    sw.WriteLine(value.ToString("N2"));
                }
            }
        }

        #endregion

        #region Private fields

        private NationalInstruments.DAQmx.Task acqTask;
        private double[] dataSingleChannel;
        private AnalogSingleChannelReader singleChReader;
        private List<double> acquisitionData;
        private List<DataPoint> oxyPlotValues = new List<DataPoint>();

        #endregion

        #region Property Changed Support
        public event PropertyChangedEventHandler PropertyChanged;

        protected void DoPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
