﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ACCBMagneticFieldTest
{
    public class ComputingUnit
    {
        public bool CalculateAvgRate(List<double> data, int acqFrequency, double watchFrequency, out double rate)
        {
            
            double sample_period = 1 / (acqFrequency - (0.53 / 86400 * acqFrequency)); //correction of the Frequency

            List<double> up_peak = new List<double>();
            List<double> down_peak = new List<double>();
            List<double> period = new List<double>();
            List<double> pulse_width = new List<double>();

            bool old_state = false;
            bool current_state = false;

            double average_up = 0;
            double average_mid = 0;

            //detect up and down peak
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i] > 1)
                    current_state = true;
                else
                    current_state = false;

                if (current_state == true && old_state == false)
                    up_peak.Add(i * sample_period);

                if (current_state == false && old_state == true)
                    down_peak.Add(i * sample_period);

                old_state = current_state;
            }

            //remove 1st peak
            if (up_peak.Count > 0)
                up_peak.RemoveAt(0);
            if (down_peak.Count > 0)
                down_peak.RemoveAt(0);

            //remove last peak 
            while (up_peak.Count != down_peak.Count)
            {
                if (up_peak.Count > down_peak.Count)
                {
                    if (up_peak.Count > 0)
                        up_peak.RemoveAt(up_peak.Count - 1);
                }
                else
                {
                    if (down_peak.Count > 0)
                        down_peak.RemoveAt(down_peak.Count - 1);
                }
            }

            if (up_peak.Count > 0)
                up_peak.RemoveAt(up_peak.Count - 1);
            if (down_peak.Count > 0)
                down_peak.RemoveAt(down_peak.Count - 1);

            //compute pulse and periode
            if (up_peak.Count > 0 && down_peak.Count > 0)
            {
                for (int i = 0; i < up_peak.Count && i<down_peak.Count; i++)
                {
                    pulse_width.Add(down_peak[i] - up_peak[i]);
                    // we have 1 less number of period than number of pulse
                    if (i < up_peak.Count - 1)
                    {
                        period.Add(up_peak[i + 1] - up_peak[i]);
                    }
                }

                for (int i = 0; i < period.Count; i++)
                {
                    average_up = average_up + period[i];
                    average_mid = average_mid + up_peak[i + 1] + pulse_width[i + 1] / 2 - (up_peak[i] + pulse_width[i] / 2);
                }
                average_up = average_up / (double)period.Count;
                average_mid = average_mid / (double)period.Count;

                double rate_up = (1.0 / watchFrequency - average_up) / (1.0 / watchFrequency) * 86400.0;

                rate = rate_up;
                return true;
            }
            else
            {
                rate = 0;
                return false;
            }
        }

    }
}
