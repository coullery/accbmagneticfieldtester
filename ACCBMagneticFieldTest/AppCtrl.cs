﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACCBMagneticFieldTest
{
    public class AppCtrl
    {

        private static AppCtrl _instance = null;
        public static AppCtrl Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AppCtrl();
                return _instance;
            }
        }
        private AppCtrl()
        {
            NICard = new NICard();
            //Configuration = new Model.Configuration();
        }

        public NICard NICard { get; set; }
        public MainWindow MainWin { get; set; }

        public void Initialize()
        {
            NICard.Initialize();

            MainWin = new MainWindow();
            MainWin.DataContext = new MainViewModel();
            MainWin.Show();

            NICard.StartAcq();
        }
    }
}
